package my

import Debug
import model.*
import model.Unit
import my.const.Const
import my.treebuilder.Leaf
import kotlin.random.Random

object MyDebug {

    fun init(game: Game, debug: Debug) {
        if(!isDebug) return

        initMs = System.currentTimeMillis()

        this.debug = debug
        this.game = game
        unitHalfWidth = (game.properties.unitSize.x / 2).toFloat()
        unitHalfHeight = (game.properties.unitSize.y / 2).toFloat()
        unitSize = Vec2Float(game.properties.unitSize.x.toFloat(), game.properties.unitSize.y.toFloat())
        unitHalfSize = Vec2Float((game.properties.unitSize.x / 2).toFloat(), (game.properties.unitSize.y / 2).toFloat())
        bulletHalfSize = 0.1f
        bulletSize = Vec2Float(0.2f, 0.2f)
    }

    const val isDebug = true

    private var initMs: Long = 0

    private lateinit var debug: Debug
    private lateinit var game: Game
    private var unitHalfWidth: Float = 0f
    private var unitHalfHeight: Float = 1f
    private lateinit var unitSize: Vec2Float
    private lateinit var unitHalfSize: Vec2Float
    private var bulletHalfSize: Float = 0f
    private lateinit var bulletSize: Vec2Float

    fun logTickTime() {
        if(!isDebug) return

        debug.draw(CustomData.Log("TickTime = ${System.currentTimeMillis() - initMs}"))
    }

    fun logActiveLeafs(leafs: List<Leaf>) {
        if(!isDebug) return

        val color = randomColor()
        for (leaf in leafs) {
            drawUnit(leaf.unit, color)
            logBullets(leaf.bullets)
        }

    }

    fun logWavePath(endLeaf: Leaf) {
        if(!isDebug) return

        var curLeaf: Leaf? = endLeaf
        while (curLeaf != null) {
            drawUnit(curLeaf.unit, randomColor())
            curLeaf = curLeaf.parentLeaf
        }
    }

    fun drawUnit(unit: Unit, color: ColorFloat) {
        val pos = Vec2Float((unit.position.x - unitHalfWidth).toFloat(), unit.position.y.toFloat())
        val cd = CustomData.Rect(pos, unitSize, color)
        debug.draw(cd)
        if(unit.health < game.properties.unitMaxHealth) {
            drawDamagedSign(unit)
        }
    }

    val WHITE = ColorFloat(1f, 1f, 1f, 1f)
    val GREEN = ColorFloat(0f, 1f, 0f, 1f)

    private fun logBullets(bullets: List<Bullet>) {
        val color = randomColor()
        for(bullet in bullets) {
            val pos = Vec2Float((bullet.position.x - bulletHalfSize).toFloat(), (bullet.position.y - bulletHalfSize).toFloat())
            val cd = CustomData.Rect(pos, bulletSize, color)
            debug.draw(cd)
        }
    }

    private fun drawDamagedSign(unit: Unit) {
        val pos = Vec2Float((unit.position.x - unitHalfWidth/2).toFloat(), (unit.position.y + unitHalfHeight / 2).toFloat())
        val cd = CustomData.Rect(pos, unitHalfSize, ColorFloat(1f, 0f, 0f, 1f))
        debug.draw(cd)
    }

    fun randomColor(): ColorFloat {
        return ColorFloat(Random.nextFloat(), Random.nextFloat(), Random.nextFloat(), 1f)
    }

    fun clear() {
        val cd = CustomData.Rect(Vec2Float(0f, 0f), Vec2Float(Const.Level.width.toFloat(), Const.Level.height.toFloat()), ColorFloat(0f, 0f, 0f, 0f))
        debug.draw(cd)
    }
}