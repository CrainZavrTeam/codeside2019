package my.treebuilder

import model.Game
import model.Level
import model.Tile
import model.Unit
import my.MyDebug
import my.const.Const
import my.ext.getTile
import my.ext.isCanMoveHorizontalEdge
import my.ext.isCanMoveVerticalEdge
import my.ext.isOnJumpPad
import kotlin.math.abs

class UnitTickActionApplyer(private val game: Game) {

    private val level: Level = game.level
    private val levelWidthDouble = Const.Level.width.toDouble()
    private val levelHeightDouble = Const.Level.height.toDouble()

    private val distEpsilon = 0.00001

    private val unitHeight = game.properties.unitSize.y
    private val unitHalfWidth = game.properties.unitSize.x / 2
    private val unitHalfHeight = game.properties.unitSize.y / 2

    private val unitJumpTime = game.properties.unitJumpTime
    private val unitJumpPadTime = game.properties.jumpPadJumpTime
    private val secondPerTick = Const.World.secondPerTick

    private var unitId = 0
    private var x = 0.0
    private var y = 0.0
    private var dx = 0.0
    private var dy = 0.0
    private var newMaxTime = 0.0
    private var newOnGround = false
    private var newCanCancel = false

    fun apply(newUnit: Unit, action: Action): Boolean {
        unitId = newUnit.id
        x = newUnit.position.x
        y = newUnit.position.y
        dx = 0.0
        dy = 0.0
        newMaxTime = newUnit.jumpState.maxTime
        newOnGround = newUnit.onGround
        newCanCancel = newUnit.jumpState.canCancel

        // вправо-влево
        when (action) {
            Action.Right -> {
                if(!moveRight())
                    return false
                if(!moveDown(isFall = false)) {
                    processJumpVertical()
                }
            }
            Action.Left -> {
                if(!moveLeft())
                    return false
                if(!moveDown(isFall = false)) {
                    processJumpVertical()
                }
            }
            Action.Up -> {
                if(newOnGround) {
                    moveJump()
                } else {
                    processJumpVertical()
                }
            }
            Action.UpRight -> {
                moveRight()
                if(newOnGround) {
                    moveJump()
                } else {
                    processJumpVertical()
                }
            }
            Action.UpLeft -> {
                moveLeft()
                if(newOnGround) {
                    moveJump()
                } else {
                    processJumpVertical()
                }
            }
            Action.Down -> {
                if(newOnGround) {
                    if(!moveDown(isFall = true))
                        return false
                } else {
                    moveDown(isFall = true)
                }
            }
            Action.DownRight -> {
                if(newOnGround) {
                    if(!moveDown(isFall = true))
                        return false
                } else {
                    moveRight()
                    moveDown(isFall = true)
                }
            }
            Action.DownLeft -> {
                if(newOnGround) {
                    if(!moveDown(isFall = true))
                        return false
                } else {
                    moveLeft()
                    moveDown(isFall = true)
                }
            }
            Action.Zero -> {
                if(!moveDown(isFall = false)) {
                    processJumpVertical()
                }
            }
        }

        checkInWall(newUnit.position.x, newUnit.position.y, dx, dy)

        newUnit.position.x += dx
        newUnit.position.y += dy

        if(newOnGround) {
            if(!isStandOn(newUnit.position.x, newUnit.position.y)) {
                newOnGround = false
                newMaxTime = 0.0
            }
        }

        newUnit.jumpState.maxTime = newMaxTime
        newUnit.jumpState.canCancel = newCanCancel
        newUnit.onGround = newOnGround

        return true
    }

    private fun processJumpPad(): Boolean {
        if(level.isOnJumpPad(x + dx - unitHalfWidth, x + dx + unitHalfWidth, y + dy)) {
            //dy = Const.World.unitJumpPadSpeedInTick
            newMaxTime = unitJumpPadTime
            newOnGround = false
            newCanCancel = false

            processJumpVertical()

            checkInWall(x, y, dx, dy)

            return true
        }
        return false
    }

    private fun moveRight(): Boolean {
        dx = Const.World.unitHorizontalSpeedInTick
        val newXR = x + dx + unitHalfWidth
        if (newXR >= levelWidthDouble) {
            dx -= newXR - levelWidthDouble + distEpsilon

            checkInWall(x, y, dx, dy)
        } else {
            val isCanMoveRight = isCanMoveHorizontally(newXR, y, unitId)
            if (!isCanMoveRight) {
                dx -= newXR - newXR.toInt() + distEpsilon

                checkInWall(x, y, dx, dy)
            }
        }
        return if(processJumpPad()) {
            true
        } else {
            abs(dx) > distEpsilon
        }
    }

    private fun moveLeft(): Boolean {
        dx = -Const.World.unitHorizontalSpeedInTick
        val newXL = x + dx - unitHalfWidth
        if(newXL < 0) {
            dx -= newXL
        } else {
            val isCanMoveLeft = isCanMoveHorizontally(newXL, y, unitId)
            if(!isCanMoveLeft) {
                dx += 1 - (newXL - newXL.toInt())
            }
        }
        return if(processJumpPad()) {
            true
        } else {
            return abs(dx) > distEpsilon
        }
    }

    private fun moveDown(isFall: Boolean): Boolean {
        if(newOnGround && !isFall) // уже стоим на земле
            return false
        if(!newCanCancel && newMaxTime > 0) // прыгаем с трамплина
            return false

        var downInterrupted = false

        if(!isFall && isStandOnLadder(x + dx, y)) {
            // уже стоим на лестнице, ничего не делаем
        } else {
            dy = -Const.World.unitFallSpeedInTick
            val newYD = y + dy
            if(newYD < 0) {
                dy -= newYD
                downInterrupted = true

                checkInWall(x + dx, y, dx, dy)
            } else {
                val isCanMoveDown = if(isFall) {
                    isCanMoveVertically(x + dx, newYD, unitId)
                } else {
                    if(isCanMoveVertically(x + dx, newYD, unitId)) {
                        if(isStandOnLadder(x + dx, newYD)) {
                            false // стоим на лестнице - не падаем
                        } else {
                            if(isStandInPlatform(x + dx, newYD)) {
                                isStandInPlatform(x + dx, y) // до движения вниз уже были в платформе - движемся сквозь платформу сбоку - можно падать,
                                // если нет, то встали на платформу - падать нельзя
                            } else {
                                true // нет ни платформы ни лестницы - можно падать
                            }
                        }
                    } else {
                        false // нельзя двтгаться вертикально - нельзя падать
                    }
                }

                if(!isCanMoveDown) {
                    if(isFall || !isStandOnLadder(x + dx, newYD)) {
                        dy += 1 - (newYD - newYD.toInt())
                    }

                    downInterrupted = true

                    checkInWall(x, y, dx, dy)
                }
            }
        }

        newMaxTime = 0.0
        newOnGround = downInterrupted
        newCanCancel = false

        if(!downInterrupted) {
            processJumpPad()
        }

        return abs(dy) > distEpsilon
    }

    private fun processJumpVertical() {
        if(newOnGround)
            return
        if(newMaxTime > distEpsilon) { // расчет вверх
            val jumpSecond = if(newMaxTime >= secondPerTick) {
                secondPerTick
            } else {
                newMaxTime
            }
            newMaxTime -= jumpSecond

            if(jumpSecond > 0) {
                val speedKf = jumpSecond / secondPerTick
                dy = (if (newCanCancel) Const.World.unitJumpSpeedInTick else Const.World.unitJumpPadSpeedInTick) * speedKf
            }

            val newYT = y + dy + unitHeight
            var jumpInterrupted = newMaxTime == 0.0
            if(newYT >= levelHeightDouble) {
                dy -= newYT - levelHeightDouble + distEpsilon
                jumpInterrupted = true

                checkInWall(x, y, dx, dy)
            } else {
                val isCanMoveUp = isCanMoveVertically(x + dx, newYT, unitId)
                if(isCanMoveUp) { // можем подняться выше
                    if(newCanCancel && isStandOnLadder(x + dx, y + dy)) { // допрыгнули до лестницы, замираем
                        jumpInterrupted = true
                        newOnGround = true
                    }

                    checkInWall(x, y, dx, dy)
                } else {
                    dy -= newYT - newYT.toInt() + distEpsilon
                    jumpInterrupted = true

                    checkInWall(x, y, dx, dy)
                }
            }
            if(jumpInterrupted) {
                newMaxTime = 0.0
                newCanCancel = false
            }
        } else {
            moveDown(isFall = false)
        }
    }

    private fun moveJump() {
        //dy = Const.World.unitJumpSpeedInTick
        newMaxTime = unitJumpTime
        newOnGround = false
        newCanCancel = true
        processJumpVertical()
    }

    private fun checkInWall(x: Double, y: Double, dx: Double, dy: Double) {
        // проверка попадания в стены
        if(MyDebug.isDebug) {
            val realX = x + dx
            val realY = y + dy
            val lb = level.getTile(realX - unitHalfWidth, realY) == Tile.WALL
            val rb = level.getTile(realX + unitHalfWidth, realY) == Tile.WALL
            val lt = level.getTile(realX - unitHalfWidth, realY + unitHeight) == Tile.WALL
            val rt = level.getTile(realX + unitHalfWidth, realY + unitHeight) == Tile.WALL
            if( lb || rb || lt || rt ) {
                val i = 0
            }
        }
    }

    private fun isStandOn(x: Double, y: Double): Boolean {
        var tile = level.getTile(x - unitHalfWidth, y - distEpsilon)
        if(tile != Tile.EMPTY && tile != Tile.JUMP_PAD)
            return true
        tile = level.getTile(x + unitHalfWidth, y - distEpsilon)
        if(tile != Tile.EMPTY && tile != Tile.JUMP_PAD)
            return true

        return false
    }

    private fun isCanMoveHorizontally(edgeX: Double, y: Double, unitId: Int): Boolean {
        val topY = y + unitHeight
        val middleY = y + unitHalfHeight
        if(level.isCanMoveVerticalEdge(edgeX, y, middleY, topY)) {
            if(!isPointsInOtherUnit(edgeX, y, edgeX, topY, unitId))
                return true
        }
        return false
    }

    private fun isPointsInOtherUnit(x1: Double, y1: Double, x2: Double, y2: Double, unitId: Int): Boolean {
        for(i in game.units.indices) {
            val otherUnit = game.units[i]
            if(otherUnit.id != unitId) {
                if(isPointInUnit(x1, y1, otherUnit) || isPointInUnit(x2, y2, otherUnit))
                    return true
            }
        }
        return false
    }

    private fun isPointInUnit(x: Double, y: Double, unit: Unit): Boolean {
        val unitPos = unit.position
        return x >= unitPos.x - unitHalfWidth && x <= unitPos.x + unitHalfWidth && y >= unitPos.y && y <= unitPos.y + unitHeight
    }

    private fun isCanMoveVertically(x: Double, edgeY: Double, unitId: Int): Boolean {
        val leftX = x - unitHalfWidth
        val rightX = x + unitHalfWidth
        if(level.isCanMoveHorizontalEdge(leftX, rightX, edgeY)) {
            if(!isPointsInOtherUnit(leftX, edgeY, rightX, edgeY, unitId))
                return true
        }
        return false
    }

    private fun isStandOnLadder(x: Double, y: Double): Boolean {
        var tile = level.getTile(x, y)
        if(tile == Tile.LADDER)
            return true
        tile = level.getTile(x, y - unitHalfHeight)
        if(tile == Tile.LADDER)
            return true

        return false
    }

    private fun isStandInPlatform(x: Double, y: Double): Boolean {
        var tile = level.getTile(x - unitHalfWidth, y)
        if(tile == Tile.PLATFORM)
            return true
        tile = level.getTile(x + unitHalfWidth, y)
        if(tile == Tile.PLATFORM)
            return true

        return false
    }
}