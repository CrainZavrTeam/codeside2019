package my.treebuilder

abstract class LeafHolderChecker(val calcScoreAndCheck: (LeafHolder, Leaf, tick: Int) -> Boolean) {

    class LastTick(scoreCalculator: LeafScoreCalculator) : LeafHolderChecker({ leafHolder, leaf, tick ->
        leaf.score = scoreCalculator.calc(leaf)

        val oldLeaf = leafHolder.get(leaf.unit.position.x, leaf.unit.position.y)
        if(oldLeaf == null || oldLeaf.tick < tick) {
            true
        } else {
            leaf.score > oldLeaf.score
        }
    })
}
