package my.treebuilder

import model.UnitAction
import model.Vec2Double
import my.const.Const

/*
class Action(
        val velocityInTickX: Double,
        val velocityInTickY: Double,
        val jump: Boolean,
        val jumpDown: Boolean,
        val needNotJump: Boolean = false
)*/

enum class Action {
    Up,
    Right,
    Left,
    Down,
    UpRight,
    UpLeft,
    DownRight,
    DownLeft,
    Zero;

    fun setMoveAction(action: UnitAction) {
        when(this) {
            Right -> action.velocity = Const.World.unitMaxHorizontalSpeed
            Left -> action.velocity = -Const.World.unitMaxHorizontalSpeed
            Up -> action.jump = true
            Down -> action.jumpDown = true
            Zero -> {}
            UpRight -> {
                action.velocity = Const.World.unitMaxHorizontalSpeed
                action.jump = true
            }
            UpLeft -> {
                action.velocity = -Const.World.unitMaxHorizontalSpeed
                action.jump = true
            }
            DownRight -> {
                action.velocity = Const.World.unitMaxHorizontalSpeed
                action.jumpDown = true
            }
            DownLeft -> {
                action.velocity = -Const.World.unitMaxHorizontalSpeed
                action.jumpDown = true
            }
        }
    }
}
