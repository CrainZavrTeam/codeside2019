package my.treebuilder

import model.Game
import java.util.*
import kotlin.collections.HashMap


class LeafHolder(gridWidth: Int) {

    private val activeLeafs: MutableList<Leaf> = LinkedList()
    private val gridLeafs: MutableMap<Int, MutableMap<Int, Leaf>> = HashMap()
    private val cellsInTile = 2

    init {
        initGridLeafs(gridWidth * cellsInTile)
    }

    fun set(x: Double, y: Double, leaf: Leaf) {
        activeLeafs.add(leaf)
        val xIdx = getIndex(x)
        val yIdx = getIndex(y)
        gridLeafs[xIdx]!![yIdx] = leaf
    }

    fun get(x: Double, y: Double): Leaf? {
        val xIdx = getIndex(x)
        val yIdx = getIndex(y)
        return gridLeafs[xIdx]!![yIdx]
    }

    private fun getIndex(levelPosition: Double): Int {
        return (levelPosition * cellsInTile).toInt()
    }

    fun getCurrentActiveLeafs(): List<Leaf> {
        return activeLeafs.toList()
    }

    fun getCurrentActiveLeafsOrderedByScore(): List<Leaf> {
        return activeLeafs.sortedByDescending { it.score }
    }

    fun clearActiveLeafs() {
        activeLeafs.clear()
    }

    private fun initGridLeafs(gridWidth: Int) {
        for(i in 0 until gridWidth)
            gridLeafs[i] = HashMap()
    }
}