package my.treebuilder

import model.*
import model.Unit
import my.ext.getIndexWhenUnitIn
import my.ext.isEnemy
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min

class LeafScoreCalculator(val game: Game) {

    // 999__9_00__000__0_00
    // hlh  w            ed
    private val healthKf =      1_000_000_000
    private val weaponKf =        100_000_000L
    private val enemyDistance =             1L

    fun calc(newLeaf: Leaf): Long {
        val healthScore = calcHealthScore(newLeaf)
        val weaponScore = calcWeaponScore(newLeaf)
        val enemyDistanceScore = calcEnemyDistanceScore(newLeaf)

        return healthScore + weaponScore + enemyDistanceScore
    }

    private fun calcEnemyDistanceScore(leaf: Leaf): Long {
        var enemyNear = false
        for(idx in leaf.units.indices) {
            val unit = leaf.units[idx]
            if(unit.isEnemy) {
                val dist = max(abs(leaf.unit.position.x - unit.position.x), abs(leaf.unit.position.y - unit.position.y))
                if(dist < 5) {
                    enemyNear = true
                    break
                }
            }
        }
        return if(enemyNear) {
            0
        } else {
            99
        }
    }

    private fun calcHealthScore(newLeaf: Leaf): Long {
        if(newLeaf.unit.health < game.properties.unitMaxHealth) {
            val pickedHealthPackIdx = newLeaf.loots.getIndexWhenUnitIn<Item.HealthPack>(newLeaf.unit)
            if(pickedHealthPackIdx >= 0) {
                val healthPack = newLeaf.loots[pickedHealthPackIdx].item as Item.HealthPack
                newLeaf.loots.removeAt(pickedHealthPackIdx)
                newLeaf.unit.health = min(newLeaf.unit.health + healthPack.health, game.properties.unitMaxHealth)
            }
        }
        return newLeaf.unit.health.toLong() * healthKf
    }

    private fun calcWeaponScore(newLeaf: Leaf): Long {
        if(isNeedCheckWeapon(newLeaf.unit)) {
            val pickedWeaponIdx = newLeaf.loots.getIndexWhenUnitIn<Item.Weapon>(newLeaf.unit)
            if(pickedWeaponIdx >= 0) {
                val weapon = newLeaf.loots[pickedWeaponIdx].item as Item.Weapon
                game.properties.weaponParams[weapon.weaponType]?.let { weaponParams ->
                    newLeaf.loots.removeAt(pickedWeaponIdx)
                    newLeaf.unit.weapon = Weapon(
                            typ = weapon.weaponType,
                            params = weaponParams,
                            fireTimer = weaponParams.reloadTime,
                            lastAngle = 0.0,
                            lastFireTick = 0,
                            magazine = weaponParams.magazineSize ?: 0,
                            spread = weaponParams.minSpread,
                            wasShooting = false
                    )
                }
            }
        }
        return getWeaponTypeScore(newLeaf.unit.weapon?.typ)
    }

    private fun getWeaponTypeScore(weaponType: WeaponType?): Long {
        return when(weaponType) {
        WeaponType.PISTOL -> 3
            WeaponType.ROCKET_LAUNCHER -> 1
            WeaponType.ASSAULT_RIFLE -> 2
            else -> 0
        } * weaponKf
    }

    fun isNeedCheckWeapon(unit: Unit): Boolean {
        val currWeaponType = unit.weapon?.typ
        return currWeaponType != WeaponType.ASSAULT_RIFLE && currWeaponType != WeaponType.PISTOL
    }

    fun isNeedSwapWeapon(newWeaponType: WeaponType, currWeaponType: WeaponType?): Boolean {
        val currWeaponScore = getWeaponTypeScore(currWeaponType)
        val weaponScore = getWeaponTypeScore(newWeaponType)
        return weaponScore > currWeaponScore
    }
}