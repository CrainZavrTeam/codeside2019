package my.treebuilder

import model.Game
import model.Tile
import model.Unit
import my.const.Const
import my.ext.getTile
import my.ext.isInRect
import my.ext.isRectIn
import my.ext.isRectInWall
import kotlin.math.sqrt

class BulletTickActionApplyer(game: Game) {

    private val secondPerTick = Const.World.secondPerTick
    private val level = game.level
    private val bulletRect = Rect()

    fun apply(newLeaf: Leaf) {
        for(bulletIdx in newLeaf.bullets.size - 1 downTo 0) {
            val bullet = newLeaf.bullets[bulletIdx]

            bulletRect.set(bullet, 2.0) // множитель для запаса

            val dx = bullet.velocity.x * secondPerTick
            val dy = bullet.velocity.y * secondPerTick
            bullet.position.x += dx
            bullet.position.y += dy

            val bulletHalfSize = bullet.size / 2
            val stepLength = sqrt(dx*dx + dy*dy)
            val stepCount = (stepLength / bulletHalfSize).toInt()
            val stepDx = dx / stepCount
            val stepDy = dy / stepCount

            // итерируем пулю внутри одного тика для лучшей точности попадания
            for(stepNo in 0 .. stepCount) {
                // check ...
                var removeBullet = !level.isRectIn(bulletRect)
                if(!removeBullet) {
                    removeBullet = level.isRectInWall(bulletRect)
                    if(!removeBullet) {
                        for(unitIdx in newLeaf.units.indices) {
                            val unit = newLeaf.units[unitIdx]
                            removeBullet = bullet.unitId != unit.id && unit.isRectIn(bulletRect)
                            if(removeBullet) {
                                if(bullet.explosionParams == null) { // в юнита попала обычная пуля, для разрывных см. ниже
                                    newLeaf.setDamage(unit, bullet.damage)
                                }
                                break
                            }
                        }
                    }

                    if(removeBullet) { // пуля куда-то попала
                        bullet.explosionParams?.let { params -> // .. разрывная
                            bulletRect.setRadius(params.radius + bullet.size / 2)
                            for(unitIdx in newLeaf.units.indices) {
                                val unit = newLeaf.units[unitIdx]
                                if(unit.isInRect(bulletRect)) {
                                    newLeaf.setDamage(unit, bullet.damage)
                                }
                            }
                        }

                        newLeaf.bullets.removeAt(bulletIdx)
                        break
                    }
                }

                bulletRect.move(stepDx, stepDy)
            }
        }
    }

}