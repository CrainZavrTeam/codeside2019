package my.treebuilder

abstract class WaveTarget(val isScoreEnough: (Leaf) -> Boolean) {

    open val waveCount: Int = -1

    class Tree : WaveTarget({ leaf ->
        false
    }) {
        override val waveCount = 10
    }
}