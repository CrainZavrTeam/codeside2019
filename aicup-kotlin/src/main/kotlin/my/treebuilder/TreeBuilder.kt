package my.treebuilder

import model.*
import model.Unit
import my.MyDebug
import my.MyDebug.GREEN
import my.const.Const


class TreeBuilder(
        buildForUnit: Unit,
        game: Game) {

    private val leafHolder = LeafHolder(Const.Level.width)
    private val actions = createActions()
    private val calcTicks = (1 / Const.World.unitHorizontalSpeedInTick).toInt()

    private val unitTickActionApplyer = UnitTickActionApplyer(game)
    private val bulletTickActionApplyer = BulletTickActionApplyer(game)
    private var startTimeMillis = 0L

    init {
        val initialLeaf = Leaf(game, buildForUnit.id)
        storeLeaf(initialLeaf)
    }

    fun buildWave(waveTarget: WaveTarget, holderChecker: LeafHolderChecker): Leaf? {
        startTimeMillis = System.currentTimeMillis()
        var bestLeaf: Leaf? = null
        val waveCount = waveTarget.waveCount
        var waveNo = 0
        do {
            waveNo++
            if(waveCount in 1 until waveNo)
                break

            val activeLeafs = leafHolder.getCurrentActiveLeafsOrderedByScore()
            //logActiveLeafs()

            leafHolder.clearActiveLeafs()
            bestLeaf = null
            for(leaf in activeLeafs) {
                for (action in actions) {
                    applyActionWave(leaf, action, holderChecker)?.let { newLeaf ->

                        if(waveTarget.isScoreEnough(leaf)) {
                            return newLeaf
                        }

                        if(newLeaf.score > bestLeaf?.score ?: Long.MIN_VALUE) {
                            bestLeaf = newLeaf
                        }
                    }
                }
            }
        }
        while (activeLeafs.isNotEmpty() && isCanLoop())

        //logActiveLeafs()

        return bestLeaf
    }

    private fun logActiveLeafs() {
        if(!MyDebug.isDebug) return

        MyDebug.logActiveLeafs(leafHolder.getCurrentActiveLeafs())
    }

    private fun logUnit(unit: Unit) {
        MyDebug.drawUnit(unit, GREEN)
    }

    private fun applyActionWave(leaf: Leaf, action: Action, holderChecker: LeafHolderChecker): Leaf? {
        val fromTick = leaf.tick + 1
        val toTick = leaf.tick + calcTicks

        val newLeaf = Leaf.copy(leaf, action, toTick)

        for(tick in fromTick .. toTick) {
            if(!unitTickActionApplyer.apply(newLeaf.unit, action))
                return null
            bulletTickActionApplyer.apply(newLeaf)
        }

        //logUnit(newLeaf.unit)

        if(!holderChecker.calcScoreAndCheck(leafHolder, newLeaf, toTick)) {
            return null
        }

        storeLeaf(newLeaf)

        return newLeaf
    }

    private fun isCanLoop(): Boolean {
        if(MyDebug.isDebug)
            return true

        val currentTimeMillis = System.currentTimeMillis()
        return currentTimeMillis - startTimeMillis < 200
    }

    private fun createActions(): Array<Action> {
        return Action.values()
        //return arrayOf(Action.Up)
        //return arrayOf(Action.Left)
        //return arrayOf(Action.UpLeft)
        //return arrayOf(Action.Left, Action.Up)
        //return arrayOf(Action.Left, Action.Zero)
        //return arrayOf(Action.Zero)
        //return arrayOf(Action.Left, Action.Up, Action.Right)
    }

    private fun storeLeaf(leaf: Leaf) {
        leafHolder.set(leaf.unit.position.x, leaf.unit.position.y, leaf)
    }
}