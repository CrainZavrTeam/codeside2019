package my.treebuilder

import model.Bullet
import model.Unit
import my.const.Const
import kotlin.math.max
import kotlin.math.min

class Rect(var xl: Double, var yb: Double, var xr: Double, var yt: Double) {

    constructor(): this(0.0, 0.0, 0.0, 0.0)

    fun move(dx: Double, dy: Double) {
        xl += dx
        xr += dx
        yb += dy
        yt += dy
    }

    fun set(bullet: Bullet, sizeKf: Double) {
        val halfSize = bullet.size / 2 * sizeKf
        val pos = bullet.position
        xl = pos.x - halfSize
        xr = pos.x + halfSize
        yb = pos.y - halfSize
        yt = pos.y + halfSize
    }

    fun set(xl: Double, yb: Double, xr: Double, yt: Double) {
        this.xl = xl
        this.xr = xr
        this.yt = yt
        this.yb = yb
    }

    fun setRadius(radius: Double) {
        val x = (xr + xl) / 2
        val y = (yt + yb) / 2
        set(
                max(x - radius, 0.0),
                max(y - radius, 0.0),
                min(x + radius, Const.Level.widthDouble),
                min(y + radius, Const.Level.heightDouble))
    }

    fun setToCenter(unit: Unit, size: Double) {
        val halfSize = size / 2
        xl = unit.position.x - halfSize
        xr = xl + size
        val centerY = unit.position.y + Const.World.unitHalfHeight
        yb = centerY - halfSize
        yt = centerY + halfSize
    }

    fun isPointIn(x: Double, y: Double): Boolean {
        return x >= xl && x <= xr && y >= yb && y <= yt
    }
}