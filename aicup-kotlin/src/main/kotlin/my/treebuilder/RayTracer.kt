package my.treebuilder

import model.Game
import model.Unit
import my.ext.isRectIn
import my.ext.isRectInWall
import my.ext.middleY
import my.vectorLength

class RayTracer(game: Game) {

    private val level = game.level
    private val traceRect = Rect()

    /**
     * @param result - здесь будут координаты конечной точки (результат трассировки)
     * @return true если достигает конечных координат
     */
    fun trace(fromUnit: Unit, size: Double, toX: Double, toY: Double, units: List<Unit>, result: TraceResult) {

        traceVector(fromUnit, size, toX, toY, toX - fromUnit.position.x, toY - fromUnit.position.y, units, result)
    }

    private fun traceVector(fromUnit: Unit, size: Double, toX: Double, toY: Double, aimX: Double, aimY: Double, units: List<Unit>, traceResult: TraceResult) {

        traceRect.setToCenter(fromUnit, size)

        val aimNormKf = 1 / vectorLength(aimX, aimY)
        val dx = aimX * aimNormKf * size
        val dy = aimY * aimNormKf * size

        traceResult.endX = fromUnit.position.x
        traceResult.endY = fromUnit.middleY

        mainLoop@ while (true) {
            if(level.isRectIn(traceRect)) {
                if(!level.isRectInWall(traceRect)) {
                    for(unitIdx in units.indices) {
                        val unit = units[unitIdx]
                        if(unit.id != fromUnit.id && unit.isRectIn(traceRect)) {
                            traceResult.endUnit = unit
                            break@mainLoop
                        }
                    }
                    traceRect.move(dx, dy)
                    traceResult.endX += dx
                    traceResult.endY += dy
                    continue
                }
            }
            break
        }
    }

    class TraceResult {
        var endX: Double = 0.0
        var endY: Double = 0.0
        var endUnit: Unit? = null
    }
}