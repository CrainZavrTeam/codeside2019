package my.treebuilder

import model.*
import model.Unit
import my.ext.copy

class Leaf(
        val parentLeaf: Leaf?,
        val rootAction: Action?,
        val level: Level,
        private val unitId: Int,
        val units: List<Unit>,
        val bullets: MutableList<Bullet>,
        val tick: Int,
        var score: Long,
        var loots: MutableList<LootBox>,
        var enemyDamage: Int) {

    val unit = units.first { it.id == unitId }

    constructor(game: Game, unitId: Int) : this(
            parentLeaf = null,
            rootAction = null,
            bullets = game.bullets.copy(),
            level = game.level,
            score = 0,
            tick = game.currentTick,
            unitId = unitId,
            units = game.units.copy(),
            loots = game.lootBoxes.copy(),
            enemyDamage = 0
    )

    fun setDamage(unit: Unit, damage: Int) {
        unit.health -= damage
        if(unit.playerId != this.unit.playerId) {
            enemyDamage += damage
        }
    }

    companion object {
        fun copy(leaf: Leaf, action: Action, tick: Int): Leaf {
            return Leaf(
                    parentLeaf = leaf,
                    rootAction = leaf.rootAction ?: action,
                    bullets = leaf.bullets.copy(),
                    level = leaf.level,
                    score = leaf.score,
                    tick = tick,
                    unitId = leaf.unitId,
                    units = leaf.units.copy(),
                    loots = leaf.loots.copy(),
                    enemyDamage = leaf.enemyDamage
            )
        }
    }
}