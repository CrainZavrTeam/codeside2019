package my

import model.Game
import model.Unit
import model.Vec2Double
import model.Weapon
import my.const.Const.World.unitHalfHeight
import my.const.Const.World.unitHeight
import my.ext.enemies
import my.ext.isEnemy
import my.ext.middleY
import my.treebuilder.RayTracer

class ShootCalculator(private val game: Game) {

    private val rayTracer = RayTracer(game)

    fun calcShootAim(unit: Unit): CalcResult {
        val weapon = unit.weapon ?: return CalcResult()
        val enemyUnitsByDistance = game.units.enemies()
                .sortedBy { distanceSqr(unit.position, it.position) }
        for(enemy in enemyUnitsByDistance) {
            calcShootAim(unit, enemy, weapon)?.let {
                return CalcResult(it, true)
            }
        }
        // не нашли по кому можно стрельнуть, нацеливаемся на центр ближайшего врага
        return if(enemyUnitsByDistance.isNotEmpty()) {
            val firstEnemy = enemyUnitsByDistance.first()
            val aim = Vec2Double(firstEnemy.position.x - unit.position.x, firstEnemy.middleY - unit.middleY)
            CalcResult(aim, false)
        } else {
            CalcResult()
        }
    }

    private fun calcShootAim(fromUnit: Unit, toUnit: Unit, weapon: Weapon): Vec2Double? {
        val targetPoints = listOf(
                Vec2Double(toUnit.position.x, toUnit.position.y + unitHalfHeight),
                Vec2Double(toUnit.position.x, toUnit.position.y + unitHeight),
                Vec2Double(toUnit.position.x, toUnit.position.y))
        val traceResult = RayTracer.TraceResult()
        for(targetPoint in targetPoints) {
            rayTracer.trace(
                    fromUnit,
                    weapon.params.bullet.size,
                    targetPoint.x,
                    targetPoint.y,
                    game.units.toList(),
                    traceResult)
            traceResult.endUnit?. let {
                if(it.isEnemy) {
                    return Vec2Double(targetPoint.x - fromUnit.position.x, targetPoint.y - fromUnit.middleY)
                }
            }
        }
        return null
    }

    class CalcResult(
            val aim: Vec2Double,
            val shoot: Boolean) {

        constructor(): this(Vec2Double(), false)
    }
}