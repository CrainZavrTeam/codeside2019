package my.ext

import model.Level
import model.Tile
import my.const.Const
import my.treebuilder.Rect

fun Level.canMove(x: Double, y: Double): Boolean {
    return getTile(x, y) != Tile.WALL
}

fun Level.isCanMoveVerticalEdge(x: Double, y1: Double, y2: Double, y3: Double): Boolean {
    return getTile(x, y1) != Tile.WALL && getTile(x, y2) != Tile.WALL && getTile(x, y3) != Tile.WALL
}

/*
fun Level.isCanStay(x1: Double, x2: Double, y: Double): Boolean {
    val tile1 = getTile(x1, y)
    val tile2 = getTile(x2, y)
    return tile1 != Tile.EMPTY || tile2 != Tile.EMPTY
}
*/

fun Level.isCanMoveHorizontalEdge(x1: Double, x2: Double, y: Double): Boolean {
    return getTile(x1, y) != Tile.WALL && getTile(x2, y) != Tile.WALL
}

fun Level.getTile(x: Double, y: Double): Tile {
    return tiles[x.toInt()][y.toInt()]
}

fun Level.isOnJumpPad(x1: Double, x2: Double, y: Double): Boolean {
    return getTile(x1, y) == Tile.JUMP_PAD || getTile(x2, y) == Tile.JUMP_PAD
}

fun Level.isRectIn(rect: Rect): Boolean {
    return rect.xl >= 0 && rect.xr > 0 && rect.xl < Const.Level.widthDouble && rect.xr < Const.Level.widthDouble &&
            rect.yt >= 0 && rect.yb > 0 && rect.yt < Const.Level.heightDouble && rect.yb < Const.Level.heightDouble
}

fun Level.isRectInWall(rect: Rect): Boolean {
    val lb = getTile(rect.xl, rect.yb) == Tile.WALL
    val rb = getTile(rect.xr, rect.yb) == Tile.WALL
    val lt = getTile(rect.xl, rect.yt) == Tile.WALL
    val rt = getTile(rect.xr, rect.yt) == Tile.WALL
    return  lb || rb || lt || rt
}

