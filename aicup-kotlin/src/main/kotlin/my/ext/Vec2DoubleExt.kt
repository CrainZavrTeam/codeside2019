package my.ext

import model.Vec2Double
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

fun Vec2Double.copy(): Vec2Double {
    return Vec2Double(x, y)
}

fun Vec2Double.length(): Double {
    return sqrt(x*x + y*y)
}

fun Vec2Double.rot(angle: Double): Vec2Double {
    val cos = cos(angle)
    val sin = sin(angle)
    return Vec2Double(x * cos - y * sin, x * sin + y * cos)
}