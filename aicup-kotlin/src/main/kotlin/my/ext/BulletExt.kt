package my.ext

import model.Bullet
import java.util.*
import kotlin.collections.ArrayList

fun Bullet.copy(): Bullet {
    return Bullet(
            weaponType = weaponType,
            damage = damage,
            explosionParams = explosionParams,
            playerId = playerId,
            position = position.copy(),
            size = size,
            unitId = unitId,
            velocity = velocity.copy()
    )
}

fun Array<Bullet>.copy(): MutableList<Bullet> {
    val list = ArrayList<Bullet>()
    for(i in 0 until size) {
        val bullet = get(i).copy()
        list.add(bullet)
    }
    return list
}

fun List<Bullet>.copy(): MutableList<Bullet> {
    val list = ArrayList<Bullet>()
    for(i in 0 until size) {
        val bullet = get(i).copy()
        list.add(bullet)
    }
    return list
}