package my.ext

import model.Weapon

fun Weapon.copy(): Weapon {
    return Weapon(
            typ = typ,
            fireTimer = fireTimer,
            lastAngle = lastAngle,
            lastFireTick = lastFireTick,
            magazine = magazine,
            params = params,
            spread = spread,
            wasShooting = wasShooting
    )
}