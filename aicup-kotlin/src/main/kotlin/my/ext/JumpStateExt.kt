package my.ext

import model.JumpState

fun JumpState.copy(): JumpState {
    return JumpState(
            canJump = canJump,
            canCancel = canCancel,
            maxTime = maxTime,
            speed = speed
    )
}