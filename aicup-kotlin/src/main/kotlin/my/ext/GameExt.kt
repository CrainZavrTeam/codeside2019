package my.ext

import model.Game

fun Game.getUnitById(id: Int): model.Unit = units.first { it.id == id }

