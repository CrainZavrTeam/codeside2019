package my.ext

import model.Game
import model.LootBox
import model.Unit
import my.const.Const

fun Array<LootBox>.copy(): MutableList<LootBox> {
    val list = ArrayList<LootBox>()
    for (i in 0 until size) {
        val lootBox = get(i).copy()
        list.add(lootBox)
    }
    return list
}

fun List<LootBox>.copy(): MutableList<LootBox> {
    val list = ArrayList<LootBox>()
    for (i in 0 until size) {
        val lootBox = get(i).copy()
        list.add(lootBox)
    }
    return list
}

fun LootBox.copy(): LootBox {
    return LootBox(
            position = position,
            item = item,
            size = size)
}

fun LootBox.isPointIn(x: Double, y: Double): Boolean {
    val halfWidth = size.x / 2
    return x >= position.x - halfWidth &&
            x <= position.x + halfWidth &&
            y >= position.y &&
            y <= position.y + size.y
}

inline fun <reified T> List<LootBox>.getIndexWhenUnitIn(unit: Unit): Int {
    for(i in indices) {
        val loot = get(i)
        if(loot.item is T) {
            if(unit.isLootIn(loot))
                return i
        }
    }
    return -1
}

inline fun <reified T> Array<LootBox>.getIndexWhenUnitIn(unit: Unit): Int {
    for(i in indices) {
        val loot = get(i)
        if(loot.item is T) {
            if(unit.isLootIn(loot))
                return i
        }
    }
    return -1
}

val LootBox.leftX get() = position.x - Const.World.lootHalfWidth
val LootBox.rightX get() = position.x + Const.World.lootHalfWidth
val LootBox.middleY get() = position.y + Const.World.lootHalfHeight
val LootBox.topY get() = position.y + Const.World.lootHeight
