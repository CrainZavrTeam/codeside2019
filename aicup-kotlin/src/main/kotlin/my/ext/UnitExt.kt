package my.ext

import model.LootBox
import model.Unit
import my.const.Const
import my.const.Const.World.unitHalfHeight
import my.const.Const.World.unitHalfWidth
import my.const.Const.World.unitHeight
import my.treebuilder.Rect
import java.util.*
import kotlin.collections.ArrayList


val model.Unit.xTile : Int
    get() = position.x.toInt()

val model.Unit.yTile : Int
    get() = position.y.toInt()

fun Unit.copy(): Unit {
    return Unit(
            playerId = playerId,
            id = id,
            health = health,
            position = position.copy(),
            size = size.copy(),
            jumpState = jumpState.copy(),
            walkedRight = walkedRight,
            stand = stand,
            onGround = onGround,
            onLadder = onLadder,
            mines = mines,
            weapon = weapon?.copy()
    )
}

val Unit.isEnemy get() = playerId != Const.playerId

fun Array<Unit>.copy(): List<Unit> {
    val list = ArrayList<Unit>()
    for (i in 0 until size) {
        val unit = get(i).copy()
        list.add(unit)
    }
    return list
}

fun List<Unit>.copy(): List<Unit> {
    val list = ArrayList<Unit>()
    for (i in 0 until size) {
        val unit = get(i).copy()
        list.add(unit)
    }
    return list
}

fun Array<Unit>.enemies(): List<Unit> {
    return filter { it.playerId != Const.playerId }
}

val Unit.isJumping get() = !jumpState.canJump

val Unit.isNonCancelJumping get() = !jumpState.canJump && !jumpState.canCancel && jumpState.maxTime > 0

val Unit.isJumpingUp get() = !jumpState.canJump && jumpState.maxTime > 0

val Unit.isJumpingDown get() = !jumpState.canJump && jumpState.maxTime == 0.0

fun Unit.isPointInUnit(x: Double, y: Double): Boolean {
    return x >= position.x - unitHalfWidth &&
            x <= position.x + unitHalfWidth &&
            y >= position.y &&
            y <= position.y + unitHeight
}

fun Unit.isRectIn(rect: Rect): Boolean {
    val lb = isPointInUnit(rect.xl, rect.yb)
    val rb = isPointInUnit(rect.xr, rect.yb)
    val lt = isPointInUnit(rect.xl, rect.yt)
    val rt = isPointInUnit(rect.xr, rect.yt)
    return  lb || rb || lt || rt
}

fun Unit.isInRect(rect: Rect): Boolean {
    val lb = rect.isPointIn(position.x - unitHalfWidth, position.y)
    val rb = rect.isPointIn(position.x + unitHalfWidth, position.y)
    val lt = rect.isPointIn(position.x - unitHalfWidth, position.y + unitHeight)
    val rt = rect.isPointIn(position.x + unitHalfWidth, position.y + unitHeight)
    return  lb || rb || lt || rt
}

val Unit.leftX get() = position.x - unitHalfWidth
val Unit.rightX get() = position.x + unitHalfWidth
val Unit.middleY get() = position.y + unitHalfHeight
val Unit.topY get() = position.y + unitHeight

fun Unit.isLootIn(loot: LootBox): Boolean {
    val lb = isPointInUnit(loot.leftX, loot.position.y)
    val rb = isPointInUnit(loot.rightX, loot.position.y)
    val lt = isPointInUnit(loot.leftX, loot.topY)
    val rt = isPointInUnit(loot.rightX, loot.topY)
    return  lb || rb || lt || rt
}

