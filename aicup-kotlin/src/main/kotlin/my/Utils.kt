package my

import model.Vec2Double
import kotlin.math.sqrt

fun vectorLength(x: Double, y: Double): Double {
    return sqrt(x*x + y*y)
}

fun distanceSqr(a: Vec2Double, b: Vec2Double): Double {
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)
}