package my.const

import model.Game
import model.Unit

object Const {

    lateinit var game: Game
    var playerId: Int = 0

    private var initiated = false

    fun init(game: Game, unit: Unit) {
        this.game = game

        if(initiated) return
        initiated = true

        playerId = unit.playerId

        Level.width = game.level.tiles.size
        Level.height = game.level.tiles[0].size
        Level.widthDouble = Level.width.toDouble()
        Level.heightDouble = Level.height.toDouble()

        World.secondPerTick = 1 / game.properties.ticksPerSecond

        World.unitMaxHorizontalSpeed = game.properties.unitMaxHorizontalSpeed

        World.unitHorizontalSpeedInTick = game.properties.unitMaxHorizontalSpeed / game.properties.ticksPerSecond
        World.unitJumpSpeedInTick = game.properties.unitJumpSpeed / game.properties.ticksPerSecond
        World.unitJumpPadSpeedInTick = game.properties.jumpPadJumpSpeed / game.properties.ticksPerSecond
        World.unitFallSpeedInTick = game.properties.unitFallSpeed / game.properties.ticksPerSecond

        World.unitWidth = game.properties.unitSize.x
        World.unitHeight = game.properties.unitSize.y
        World.unitHalfWidth = game.properties.unitSize.x / 2
        World.unitHalfHeight = game.properties.unitSize.y / 2

        World.lootWidth = game.properties.lootBoxSize.x
        World.lootHeight = game.properties.lootBoxSize.y
        World.lootHalfWidth = game.properties.lootBoxSize.x / 2
        World.lootHalfHeight = game.properties.lootBoxSize.y / 2
    }

    object Level {
        var width = 0
        var height = 0
        var widthDouble = 0.0
        var heightDouble = 0.0
    }

    object World {
        var secondPerTick = 0.0

        var unitHorizontalSpeedInTick = 0.0
        var unitJumpSpeedInTick = 0.0
        var unitJumpPadSpeedInTick = 0.0
        var unitFallSpeedInTick = 0.0

        var unitMaxHorizontalSpeed = 0.0

        var unitWidth = 0.0
        var unitHeight = 0.0
        var unitHalfWidth = 0.0
        var unitHalfHeight = 0.0

        var lootWidth = 0.0
        var lootHeight = 0.0
        var lootHalfWidth = 0.0
        var lootHalfHeight = 0.0
    }
}