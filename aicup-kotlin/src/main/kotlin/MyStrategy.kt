import model.*
import my.MyDebug
import my.ShootCalculator
import my.const.Const
import my.ext.getIndexWhenUnitIn
import my.treebuilder.LeafHolderChecker
import my.treebuilder.LeafScoreCalculator
import my.treebuilder.TreeBuilder
import my.treebuilder.WaveTarget

class MyStrategy {

    fun getAction(unit: model.Unit, game: Game, debug: Debug): UnitAction {

        Const.init(game, unit)
        MyDebug.init(game, debug)

        var unitAction = UnitAction(
                velocity = 0.0,
                aim = Vec2Double(0.0, 0.0),
                jump = false,
                jumpDown = false,
                plantMine = false,
                shoot = false,
                swapWeapon = false,
                reload = false
        )

        setActionMain(unit, game, debug, unitAction)
        //testAction(unit, game, debug, unitAction)

        MyDebug.logTickTime()

        return unitAction
    }

    private fun testAction(unit: model.Unit, game: Game, debug: Debug, unitAction: UnitAction) {
        // перебор вариантов
        val scoreCalculator = LeafScoreCalculator(game)
        val holderChecker = LeafHolderChecker.LastTick(scoreCalculator)
        val waveTarget = WaveTarget.Tree()
        var newUnit: model.Unit? = null
        TreeBuilder(unit, game).buildWave(waveTarget, holderChecker)?.let { leaf ->
            //MyDebug.logWavePath(leaf)
            newUnit = leaf.unit
            leaf.rootAction?.setMoveAction(unitAction)
        }
    }

    private fun setActionMain(unit: model.Unit, game: Game, debug: Debug, unitAction: UnitAction) {

        // перебор вариантов
        val scoreCalculator = LeafScoreCalculator(game)
        val holderChecker = LeafHolderChecker.LastTick(scoreCalculator)
        val waveTarget = WaveTarget.Tree()
        var newUnit: model.Unit? = null
        TreeBuilder(unit, game).buildWave(waveTarget, holderChecker)?.let { leaf ->
            //MyDebug.logWavePath(leaf)
            newUnit = leaf.unit
            leaf.rootAction?.setMoveAction(unitAction)
        }

        // смена оружия
        if(scoreCalculator.isNeedCheckWeapon(unit)) {
            val pickedWeaponIdx = game.lootBoxes.getIndexWhenUnitIn<Item.Weapon>(unit)
            if(pickedWeaponIdx >= 0) {
                val weapon = game.lootBoxes[pickedWeaponIdx].item as Item.Weapon
                unitAction.swapWeapon = scoreCalculator.isNeedSwapWeapon(weapon.weaponType, unit.weapon?.typ)
            }
        }

        // стрельба
        ShootCalculator(game).calcShootAim(unit).let {
            unitAction.aim = it.aim
            unitAction.shoot = it.shoot
        }

        // перезарядка
        unitAction.reload = unit.weapon?.magazine == 0
    }
}
